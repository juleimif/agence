package com.example.acer.agence.domain.presenter;

import android.util.Log;

import com.example.acer.agence.R;
import com.example.acer.agence.data.view.LoginContract;
import com.example.acer.agence.domain.model.User;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;


public class LoginPresenter implements LoginContract.Presenter {
    LoginContract.View mView;

    @Inject
    public LoginPresenter(LoginContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadDatUser(Realm realm, String email, String password) {
        RealmResults<User> realmObjects = realm.where(User.class).findAll();
        for (User myRealmObject : realmObjects) {
            if (!email.equals(myRealmObject.getName())) {
                mView.showError(mView.getContext().getResources().getString(R.string.input_error_user));
            }
            else
            if (!password.equals(myRealmObject.getPassword())) {
                mView.showError(mView.getContext().getResources().getString(R.string.input_error_password));
            } else
                mView.sucessLogin(email);
        }

    }

    @Override
    public void saveData(Realm realm,String email, String password) {
        User user = new User();
        user.setId((int) (realm.where(User.class).findAll().size() + 1 + System.currentTimeMillis()));
        user.setName(email);
        user.setPassword(password);

        // Persist your data easily
        realm.beginTransaction();
        realm.copyToRealm(user);
        realm.commitTransaction();
    }
}