package com.example.acer.agence.data.injector.module;

import com.example.acer.agence.data.view.LoginContract;
import com.example.acer.agence.data.injector.scope.CustomScope;
import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    private final LoginContract.View mView;

    public LoginModule(LoginContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    LoginContract.View providesMainScreenContractView() {
        return mView;
    }

}
