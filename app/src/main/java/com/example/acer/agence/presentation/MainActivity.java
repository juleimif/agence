package com.example.acer.agence.presentation;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.content.Context;
import android.widget.TextView;

import com.example.acer.agence.utils.AgenceConstants;
import com.example.acer.agence.presentation.activities.BaseActivity;
import com.example.acer.agence.R;
import com.example.acer.agence.presentation.activities.LoginActivity;
import com.example.acer.agence.utils.PreferencesSession;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.nav_view)
    NavigationView nav_view;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webView)
    WebView mWebview;
    private ActionBarDrawerToggle toggle;
    private String user;
    private PreferencesSession preferencesSession;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        initView();
    }

    public void initView() {

        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(R.string.app_name);

        user = getIntent().getExtras().getString("user");

        preferencesSession = new PreferencesSession(this);

        toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(this);
        nav_view.setItemIconTintList(null);

        View header = nav_view.getHeaderView(0);
        TextView tv_user = (TextView) header.findViewById(R.id.tv_user);
        tv_user.setText("¡Hello " + user + "!");

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_view) {
            showWebView();
        }

        if (id == R.id.nav_notification) {
            sendNotification();
        }

        if (id == R.id.nav_log_out) {
            logout();
        }

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers();
        }
        return true;
    }

    private void showWebView() {

        Log.i(TAG, "showWebView: ");
        WebSettings webSettings = mWebview.getSettings();
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);

        mWebview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        mWebview.loadUrl(AgenceConstants.LOCAL_URL);

    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void sendNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AgenceConstants.LOCAL_URL));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.drawable.ic_mensajes);
        mBuilder.setContentTitle("Notification");
        mBuilder.setContentText("Agence Test");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public void logout() {
        preferencesSession.clearPreferences();
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        finish();
    }
}
