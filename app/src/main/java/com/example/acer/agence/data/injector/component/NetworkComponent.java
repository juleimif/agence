package com.example.acer.agence.data.injector.component;
import com.example.acer.agence.data.injector.module.NetworkModule;
import com.example.acer.agence.data.injector.module.AppModule;
import javax.inject.Singleton;
import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {
    Retrofit retrofit();
}