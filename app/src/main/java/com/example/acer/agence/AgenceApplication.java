package com.example.acer.agence;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import com.example.acer.agence.data.injector.component.DaggerNetworkComponent;
import com.example.acer.agence.data.injector.module.AppModule;
import com.example.acer.agence.data.injector.component.NetworkComponent;
import com.example.acer.agence.data.injector.module.NetworkModule;

/**
 * Created by Acer on 7/3/2018.
 */

public class AgenceApplication extends Application {
    private NetworkComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        //DbRealm
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("agenceDb.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);

        //Dagger
        mNetComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }
    public NetworkComponent getNetComponent() {
        return mNetComponent;
    }

}
