package com.example.acer.agence.data.injector.component;
import com.example.acer.agence.data.injector.module.LoginModule;
import com.example.acer.agence.presentation.activities.LoginActivity;
import com.example.acer.agence.data.injector.scope.CustomScope;

import dagger.Component;

    @CustomScope
    @Component(dependencies = NetworkComponent.class, modules = LoginModule.class)
    public interface LoginComponent {
        void inject(LoginActivity fragment);
    }

