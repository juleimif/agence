package com.example.acer.agence.presentation.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.acer.agence.data.injector.component.DaggerLoginComponent;
import com.example.acer.agence.data.injector.module.LoginModule;
import com.example.acer.agence.data.view.LoginContract;
import com.example.acer.agence.domain.presenter.LoginPresenter;
import com.example.acer.agence.AgenceApplication;
import com.example.acer.agence.utils.PreferencesSession;
import com.example.acer.agence.R;
import com.example.acer.agence.utils.Utils;
import com.example.acer.agence.presentation.MainActivity;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;

public class LoginActivity extends BaseActivity implements Validator.ValidationListener, LoginContract.View {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @NotEmpty(message = "Debe ingresar su usuario")
    @BindView(R.id.input_user)
    TextInputEditText input_user;
    @NotEmpty(message = "Debe ingresar su contraseña")
    @BindView(R.id.input_password)
    TextInputEditText input_contraseña;
    @BindView(R.id.ly_login)
    CoordinatorLayout ly_login;
    @BindView(R.id.cb_remember)
    CheckBox ch_recordar;

    private Validator validator;
    private PreferencesSession preferencesManager;
    private Realm realm;

    @Inject
    LoginPresenter loginPresenter;

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        preferencesManager = new PreferencesSession(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        realm = Realm.getDefaultInstance();
        initComponet();
        getStatusCheck();

        //Save data
        loginPresenter.saveData(realm, "Damian", "12345");

    }

    public void initComponet() {
        DaggerLoginComponent.builder()
                .networkComponent(((AgenceApplication) getApplication()).getNetComponent())
                .loginModule(new LoginModule(this))
                .build().inject(this);

    }

    @OnClick({R.id.iv_salir, R.id.btn_singUp, R.id.cb_remember})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_salir:
                finish();
                break;
            case R.id.btn_singUp:
                hideKeyboard();
                validator.validate();
                break;
            case R.id.cb_remember:
                if (!ch_recordar.isChecked()) {
                    preferencesManager.clearPreferences();
                }
                break;
        }
    }


    @Override
    public void onValidationSucceeded() {
        if (ch_recordar.isChecked()) {

            preferencesManager.setUserEmail(input_user.getText().toString());
            preferencesManager.setUserPassword(input_contraseña.getText().toString());
        }
        loginPresenter.loadDatUser(realm, input_user.getText().toString(), input_contraseña.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    protected void hideKeyboard() {

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Valodate status sesion
     */
    public void getStatusCheck() {

        if (!preferencesManager.getUserEmail().equals("") && !preferencesManager.gettUserPassword().equals("")) {
            Log.i(TAG, "getStatusCheck: " + preferencesManager.getUserEmail());
            input_user.setText(preferencesManager.getUserEmail());
            input_contraseña.setText(preferencesManager.gettUserPassword());
            ch_recordar.setChecked(true);
        }
    }


    @Override
    public void sucessLogin(String user) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
    }

    @Override
    public void showError(String message) {
        Utils.showSnackBar(this, message, ly_login);
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
}
