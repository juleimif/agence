package com.example.acer.agence.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferencesSession {
    private final String TAG = PreferencesSession.class.getSimpleName();

    private static final String KEY_USER = "USER";
    private static final String KEY_USER_PASSWORD = "USER_PASSWORD";
    private static final String PREFER_NAME = "Preference";


    public SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Constructor
    public PreferencesSession(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }


    public void clearPreferences(){
        setUserEmail("");
        setUserPassword("");

    }

    public void setUserEmail(String userEmail){
        editor.putString(KEY_USER, userEmail);
        editor.apply();
        editor.commit();
    }
    public String getUserEmail() {return pref.getString(KEY_USER, "");}

    public void setUserPassword(String userPassword){
        editor.putString(KEY_USER_PASSWORD, userPassword);
        editor.apply();
        editor.commit();
    }

    public String gettUserPassword() {return pref.getString(KEY_USER_PASSWORD, "");}


}
