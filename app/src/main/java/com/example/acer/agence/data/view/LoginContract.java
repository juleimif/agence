package com.example.acer.agence.data.view;


import android.content.Context;

import io.realm.Realm;

public interface LoginContract {

    interface View {
        void sucessLogin(String email);

        void showError(String message);

        Context getContext();
    }

    interface Presenter {
        void loadDatUser(Realm realm, String email, String password);
        void saveData(Realm realm, String email, String password);
    }
}

