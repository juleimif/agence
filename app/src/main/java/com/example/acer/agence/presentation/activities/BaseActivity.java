package com.example.acer.agence.presentation.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getName();
    ProgressDialog progress;
    abstract public int getLayout();
    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
//        Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);
        onCreateView(savedInstanceState);
    }

    public void showProgressDialogB(Context context){
        Log.i(TAG, "showProgressDialogB: " + context);
        progress = new ProgressDialog(context);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();

    }

    public void dismissDialog(){
        if(progress != null){
            progress.dismiss();
        }
    }
}
